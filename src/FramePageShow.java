import javax.swing.*;
import java.awt.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class FramePageShow extends MainFrame {
    private JTextArea textAreaData;

    public void show() {
        EventQueue.invokeLater(() -> {
            JFrame frame = new JFrame("Show");
            frame.setSize(600, 700);
            frame.setMinimumSize(new Dimension(300, 700));
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

            JPanel panel = new JPanel(new GridLayout(1, 1, 5, 5));

            textAreaData = new JTextArea();
            textAreaData.setFont(mainFont);
            textAreaData.setEditable(false);

            JScrollPane scrollPane = new JScrollPane(textAreaData);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

            panel.add(scrollPane);

            frame.getContentPane().add(panel);
            frame.pack();
            frame.setLocationByPlatform(true);
            frame.setVisible(true);
            frame.setResizable(false);

            showTabel();
        });
    }

    public void showTabel() {
        try {
            Class.forName(JDBC_DRIVER);
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM barang");

            StringBuilder data = new StringBuilder();
            int i = 1;
            while (rs.next()) {
                data.append("Data ke-").append(i).append("\n");
                data.append("Kode Barang: ").append(rs.getString("kode_brg")).append("\n");
                data.append("Nama Barang: ").append(rs.getString("nm_brg")).append("\n");
                data.append("Satuan: ").append(rs.getString("satuan")).append("\n");
                data.append("Stok: ").append(rs.getString("stok_brg")).append("\n");
                data.append("Stok minimal: ").append(rs.getString("stok_min")).append("\n\n");
                i++;
            }

            textAreaData.setText(data.toString());

            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
